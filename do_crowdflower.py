#!/usr/bin/env python

import crowdflower
import sys
import json

def get_contents(filename):
    with open(filename) as f:
        return f.read();

def create_job(conn, data_filename):
    job_params = {
        "title": "Part of Speech Labeling",
        "included_countries": ["US", "GB"],
        "payment_cents": 10,
        "judgments_per_unit": 5,
        "instructions": get_contents("instructions.html"),
        "support_email": "example@example.com",
        "pages_per_assignment" : 1,
        "units_per_assignment": 20,
        "cml": get_contents("template.cml"),
        "css": get_contents("template.css")
    }

    with open(data_filename) as data_file:
        data = json.load(data_file)
        print "Uploading %d questions" % len(data)
        job = conn.upload(data)

    update_result = job.update(job_params)

    if "errors" in update_result:
        print update_result["errors"]
        exit()

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "Usage: %s %s" % (sys.argv[0], "<data_filename>")
        sys.exit(1)
    conn = crowdflower.Connection()
    create_job(conn, sys.argv[1])
