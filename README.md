# Crowdflower Example

## Introduction
This is a simple example of how to annotate text using Crowdflower, using a stripped-down part-of-speech tagging task.

## Installation
* Get the Crowdflower API from PyPI:
    *  `easy_install -U crowdflower`
* Or from GitHub:
    * `git clone https://github.com/peoplepattern/crowdflower.git`
    * `cd crowdflower`
    * `python setup.py develop`
* Get an API key from https://make.crowdflower.com/account/user
* Set the environment variable `CROWDFLOWER_API_KEY` to the API key

## Uploading
* Run `python do_crowdflower.py data.json`
* The task along with the data will be uploaded to Crowdflower
* Use their browser UI to preview the job, upload test questions (crucial!), and launch the job